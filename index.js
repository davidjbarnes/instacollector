require('dotenv').config();

const { google } = require('googleapis');
const path = require('path');
const puppeteer = require('puppeteer');
const fs = require('fs');

const USERNAME = process.env.INSTA_USERNAME;
const PASSWORD = process.env.INSTA_PASSWORD;
const SAVED_URL = process.env.INSTA_SAVED_URL;

(async () => {
	let images = [];
	let lastImg = fs.readFileSync('config.txt', { encoding: 'utf8' });
	console.log('process.env=', process.env);
	console.log('lastImg=', lastImg);

	const browser = await puppeteer.launch({ headless: false, args: ['--no-sandbox', '--disable-setuid-sandbox'] });
	const page = await browser.newPage();

	await page.goto('https://instagram.com');

	await page.waitForSelector('input[name=username]');
	await page.type('input[name=username]', USERNAME);
	await page.type('input[name=password]', PASSWORD);

	await Promise.all([
		page.click('button[type=submit]'),
		page.waitForNavigation({ waitUntil: 'networkidle2' })
	]);

	await Promise.all([
		page.goto(SAVED_URL),
		page.waitForNavigation({ waitUntil: 'networkidle2' })
	]);

	const hrefs = await page.$$eval("a", (list) => list.map((elm) => elm.href));
	console.log('hrefs=', hrefs);

	if (hrefs.length > 0) {
		for (i = 0; i < hrefs.length; i++) {
			const item = hrefs[i];
			if (item.includes("/p/")) {
				const imageName = item.split('/')[4].concat('.png');
				console.log('imageName', imageName);
				if (imageName == lastImg) break;
				images.push(item + 'media?size=l')
			}
		};

		if (images.length > 0) {
			for (i = 0; i < images.length; i++) {
				const image = images[i];
				const viewSource = await page.goto(image);
				const imageName = image.split('/')[4].concat('.png');
				fs.writeFile('./images/' + imageName, await viewSource.buffer(), (err) => console.error(err));
			}

			lastImg = images[0].split('/')[4].concat('.png');
			fs.writeFileSync('config.txt', lastImg);
			console.log("Last image=" + lastImg);
		} else {
			console.log('All images are up to date!');
		}
	}

	await browser.close();
})();
